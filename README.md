UQTR-TP01-PIOCHE

CLASS: DECK, CARD, PLAYER
ABSTRACT CLASS: AUTOPLAY

CARD:
	Suit enum (Diamonds, Hearts, Spades, Clubs)
	Constructor
	Getters;
	
DECK:
	Stack data struct (push/pop/peek ops)
	Generate cards
	Shuffle cards
	
AUTOPLAY:
	//INIT
	Ask # players
	Generate players
	Generate order
	Generate full deck+shuffle
	Generate empty deck
	Push cards to players
	
	//PLAY
	