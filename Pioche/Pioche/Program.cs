﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pioche
{
    class Program
    {
        static void Main(string[] args)
        {
            //Creating the list of players (2-4)
            Random randomGen = new Random();
            byte playerQty = (byte)(randomGen.Next(3) + 2);
            Console.WriteLine("Starting a game of \"Pioche\" with " + playerQty + " players.");
            Player[] players = new Player[playerQty];
            for (int i = 0; i < playerQty; i++)
                players[i] = new Player();

            //Creating the game decks
            Deck primaryDeck = new Deck(); //Populated deck with cards to push to players
            Deck secondaryDeck = new Deck(); //Empty deck receiving the poped cards from players
            primaryDeck.Populate();
            primaryDeck.Shuffle(52);

            //Sending 8 cards to all players
            for (int i = 0; i < playerQty; i++)
                for (int j = 0; j < 8; j++)
                    players[i].AddCard(primaryDeck.Pop());

            //Printing players & hands
            foreach (Player player in players)
                Console.WriteLine(player);

            //Game loop
            secondaryDeck.Push(players[0].PlayFirst());
            byte turn = 1;
            Card cardAtPlay;
            while (true)
            {
                //Playing card
                Console.WriteLine(players[turn].GetName() + " must either play: {0} or {1}s.", secondaryDeck.Peek().GetRank(), secondaryDeck.Peek().GetSuit());
                cardAtPlay = players[turn].Play(secondaryDeck.Peek());
                //Checking if the card is in the player's hands and playing it
                if (cardAtPlay == null)
                {
                    Console.WriteLine(players[turn].GetName() + " has no card to play. Picking up " + primaryDeck.Peek() + " from the game deck.");
                    players[turn].AddCard(primaryDeck.Pop());
                }
                else
                {
                    secondaryDeck.Push(cardAtPlay);
                }
                //Checking if the current player has won the game
                if (players[turn].CheckHands())
                {
                    Console.WriteLine(players[turn].GetName() + " has won the game!");
                    break;
                }
                else
                {
                    //Checking if the Primary Deck is empty and switching Decks if true
                    if (primaryDeck.IsEmpty())
                    {
                        Console.WriteLine("The primary Deck is empty. Shuffled Secondary Deck now being used as the Primary Deck.");
                        while (!secondaryDeck.IsEmpty())
                            primaryDeck.Push(secondaryDeck.Pop());
                        secondaryDeck.Push(primaryDeck.Pop());
                        primaryDeck.Shuffle(52);
                    }
                    //Changing player
                    if (turn == playerQty - 1)
                        turn = 0;
                    else
                        turn++;
                }
                System.Threading.Thread.Sleep(1000);
            }

            Console.ReadKey();
        }
    }
}
