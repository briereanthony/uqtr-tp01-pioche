﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pioche
{
    class Player
    {
        //Class Variables Initialization
        private static List<string> nomJoueur = new List<string> { "Pedro Martinez", "Louis Cyr", "Bobby McFarlane", "Yvan Deschaussures", "Rico Suave", "Peter Pete", "Simon Simoneau", "Barry Allen" };
        private List<Card> hand = new List<Card>();
        private List<Card> stackPlay = new List<Card>();
        static Random random = new Random();
        public string joueur;
        private string returnValue;

        //Joueur Aléatoire Constructeur
        public Player()
        {
            int rnd = random.Next(nomJoueur.Count());
            joueur = nomJoueur[rnd];
            nomJoueur.RemoveAt(rnd);
        }

        //Ajout carte a la main du joueur
        public void AddCard(Card card)
        {
            hand.Add(card);
        }

        //Compare si jeux possible
        public Card Play(Card card)
        {
            for (int i = 0; i < hand.Count(); i++)
            {
                if (hand[i].GetRank().Equals(card.GetRank()) || hand[i].GetSuit().Equals(card.GetSuit()))
                {
                    Card toPush = hand[i];
                    hand.RemoveAt(i);
                    Console.WriteLine(joueur + " plays " + toPush + ".");
                    return toPush;
                }
            }
            return null;
        }
        public Card PlayFirst()
        {
            Card toPush = hand[0];
            hand.RemoveAt(0);
            Console.WriteLine(joueur + " plays " + toPush + ".");
            return toPush;
        }

        //ToString Redefine
        public override string ToString()
        {
            returnValue = joueur + " - ";
            for (int i = 0; i < hand.Count(); i++)
            {
                returnValue += hand[i];
                if (i < hand.Count() - 1)
                    returnValue += ", ";
            }
            return returnValue;
        }

        public string GetName()
        {
            return joueur;
        }

        public bool CheckHands()
        {
            if (hand.Count() == 0)
                return true;
            return false;
        }
    }
}