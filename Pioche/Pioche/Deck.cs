﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pioche
{
    class Deck
    {
        //Class Variables Initialization
        private sbyte top = -1;
        private const byte DECK_SIZE = 52;
        private Card[] stack;

        //Deck Constructor
        public Deck()
        {
            this.stack = new Card[DECK_SIZE];
        }

        //Populate Deck
        public void Populate()
        {
        for (byte suitSwitch = 0; suitSwitch < 4; suitSwitch++)
            for (byte value = 1; value < 14; value++)
                this.Push(new Card((Card.CardSuit)suitSwitch, (Card.Rank)value));
        }

        //Shuffle Deck
        public void Shuffle(int shuffles)
        {
            Random randomGen = new Random();
            int firstRand, secondRand;
            Card temp;
            for (int i = 0; i < shuffles; i++)
            {
                firstRand = randomGen.Next(DECK_SIZE);
                secondRand = randomGen.Next(DECK_SIZE);
                temp = stack[firstRand];
                stack[firstRand] = stack[secondRand];
                stack[secondRand] = temp;
            }
        }

        //Stack Data Structure Interactions
        public void Push(Card toPush)
        {
            stack[++top] = toPush;
        }

        public Card Pop()
        {
            if (IsEmpty())
                return null;
            return stack[top--];
        }

        public Card Peek()
        {
            if (IsEmpty())
                return null;
            return stack[top];
        }

        public bool IsEmpty()
        {
            if (top < 0)
                return true;
            return false;
        }

        public bool IsFull()
        {
            if (top == DECK_SIZE - 1)
                return true;
            return false;
        }
    }
}
