﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pioche
{

    class Card
    {
        public enum CardSuit { Diamond = 0, Heart, Club, Spade }
        public enum Rank { Ace = 1, Deuce, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King }
        
        //Class variables
        private CardSuit suit;
        private Rank rank;

        //Constructor
        public Card(CardSuit suit, Rank rank){
            this.suit = suit;
            this.rank = rank;
        }

        //Getters
        public CardSuit GetSuit()
        {
            return suit;
        }
        public Rank GetRank()
        {
            return rank;
        }

        //ToString Override
        public override string ToString()
        {
            return rank + " of " + suit + "s";
        }
    }
}
